import React, { Component } from "react";
import Header from "./components/header/Header";
import Home from "./components/home/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Expenses from "./components/expense/Expenses";
import ExpenseSubmit from "./components/expense/ExpenseSubmit";
import EarningSubmit from "./components/earning/EearningSubmit";
import Earnings from "./components/earning/Earnings";
import Auth from "./components/auth/Auth";
import Profit from "./components/profit/Profit";
import { fetchFullProfit, fetchYears } from "./redux/actions";
import { connect } from "react-redux";
import ExpenseEdit from "./components/expense/ExpenseEdit";
import EarningEdit from "./components/earning/EarningEdit";
import "./css/app.css";

class App extends Component {
  componentDidMount() {
    this.props.fetchFullProfit();
    this.props.fetchYears();
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Header />
          <Auth />
          <div className="content container">
            <Switch>
              <Route exact path="/expenses" component={Expenses} />
              <Route exact path="/expenses/add" component={ExpenseSubmit} />
              <Route exact path="/expenses/edit" component={ExpenseEdit} />
              <Route exact path="/earnings" component={Earnings} />
              <Route exact path="/earnings/add" component={EarningSubmit} />
              <Route exact path="/earnings/edit" component={EarningEdit} />
              <Route exact path="/profit" component={Profit} />
              <Route exact path="/" component={Home} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

const mapDispatchToprops = {
  fetchFullProfit,
  fetchYears,
};

export default connect(null, mapDispatchToprops)(App);
