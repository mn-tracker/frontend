import $ from "jquery";
import * as moment from "moment";

export function setYears(setState, fetchYears, fetchExpenseAndEarningPerMonth) {
  var years = JSON.parse(localStorage.getItem("years"));
  if (years !== null) {
    if (setState !== null) setState({ years: years });
  } else fetchYears();

  var year = JSON.parse(localStorage.getItem("statsYear"));
  if (year !== null) {
    if (setState !== null) setState({ year: year });
    fetchExpenseAndEarningPerMonth(year);
  } else if (years !== null) {
    fetchExpenseAndEarningPerMonth(years[0]);
  }
}

export function setProfit(profit) {
  var profitClass = "";
  if (profit > 0) profitClass = "profit-plus";
  if (profit < 0) profitClass = "profit-minus";

  var currency = localStorage.getItem("currency");

  if (currency === null) currency = "";
  return {
    profitValue:
      profit > 0 ? "+" + profit + " " + currency : profit + " " + currency,
    profitClass: profitClass,
  };
}

export function setInStorage(name, value) {
  localStorage.setItem(name, JSON.stringify(value));
}

export function getDateFilter(name, setState, fetchData, paramPrefixes) {
  var dateFilter = JSON.parse(localStorage.getItem(name + "DateFilter"));

  if (dateFilter !== null) {
    const newDateStart = new Date(dateFilter.dateStart);
    const newDateEnd = new Date(dateFilter.dateEnd);

    setState(
      {
        dateStart: newDateStart != "Invalid Date" ? newDateStart : null,
        dateEnd: newDateEnd != "Invalid Date" ? newDateEnd : null,
        dateStartParam:
          newDateStart != "Invalid Date"
            ? paramPrefixes[0] +
              "dateStart=" +
              moment(newDateStart, "DD-MM-YYYY").format().split("T")[0]
            : "",
        dateEndParam:
          newDateEnd != "Invalid Date"
            ? paramPrefixes[1] +
              "dateEnd=" +
              moment(newDateEnd, "DD-MM-YYYY").format().split("T")[0]
            : "",
      },
      function () {
        fetchData(true);
        $(".block-title").toggleClass("block-active").next().slideToggle(150);
      }
    );
  } else {
    fetchData(true);
  }
}

export function setDateFilter(name, dateStart, dateEnd, fetchData) {
  if (dateStart !== null || dateEnd !== null) {
    fetchData(true);

    if (dateStart === null) {
      setInStorage(name + "DateFilter", {
        dateEnd: moment(dateEnd, "DD-MM-YYYY").format(),
      });
    } else {
      setInStorage(name + "DateFilter", {
        dateStart: moment(dateStart, "DD-MM-YYYY").format(),
      });
    }

    if (dateStart !== null && dateEnd !== null)
      setInStorage(name + "DateFilter", {
        dateStart: moment(dateStart, "DD-MM-YYYY").format(),
        dateEnd: moment(dateEnd, "DD-MM-YYYY").format(),
      });
  }
}
