import Cookies from "universal-cookie";

export const setCookie = (name, data) => {
  const cookies = new Cookies();

  cookies.set(name, data, { maxAge: 3600 * 24 * 7 }, { path: "/" });
};

export const getCookie = (name) => {
  const cookies = new Cookies();
  return cookies.get(name);
};

export const deleteToken = () => {
  const cookies = new Cookies();
  cookies.remove("access_token");
};
