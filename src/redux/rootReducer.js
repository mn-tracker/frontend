import { combineReducers } from "redux";
import { appReducer } from "./appReducer";
import { expenseReducer } from "./expenseReducer";
import { earningReducer } from "./earningReducer";
import { authReducer } from "./authReducer";
import { statisticReducer } from "./statisticReducer";
import { expenseSortParamsReducer } from "./expenseSortParamsReducer";
import { earningSortParamsReducer } from "./earningSortParamsReducer";

export const rootReducer = combineReducers({
  app: appReducer,
  expenses: expenseReducer,
  expenseSortParams: expenseSortParamsReducer,
  earningSortParams: earningSortParamsReducer,
  earnings: earningReducer,
  auth: authReducer,
  statistics: statisticReducer,
});
