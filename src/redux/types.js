export const FETCH_EXPENSES = "FETCH_EXPENSES";
export const FETCH_EARNINGS = "FETCH_EARNINGS";
export const AUTH_TYPE = "AUTH_TYPE";
export const SIGNUP_ALERTS = "SIGNUP_ALERTS";
export const REMOVE_SIGNUP_ALERTS = "REMOVE_SIGNUP_ALERTS";
export const LOGIN_ALERTS = "LOGIN_ALERTS";
export const REMOVE_LOGIN_ALERTS = "REMOVE_LOGIN_ALERTS";
export const SET_LOGGED = "SET_LOGGED";
export const REMOVE_LOGGED = "REMOVE_LOGGED";
export const FETCH_CATEGORIES = "FETCH_CATEGORIES";
export const ADD_EXPENSE = "ADD_EXPENSE";
export const FETCH_PIE_EXPENSE_STATISTIC = "FETCH_PIE_EXPENSE_STATISTIC";
export const FETCH_EXPENSE_AND_EARNING_PER_MONTH =
  "FETCH_EXPENSE_AND_EARNING_PER_MONTH";
export const ADD_EARNING = "ADD_EARNING";
export const SET_PROFIT_VALUE = "SET_PROFIT_VALUE";
export const SHOW_EXPENSE_SUBMIT_LOADER = "SHOW_EXPENSE_SUBMIT_LOADER";
export const HIDE_EXPENSE_SUBMIT_LOADER = "HIDE_EXPENSE_SUBMIT_LOADER";
export const SHOW_EARNING_SUBMIT_LOADER = "SHOW_EARNING_SUBMIT_LOADER";
export const HIDE_EARNING_SUBMIT_LOADER = "HIDE_EARNING_SUBMIT_LOADER";
export const SHOW_EXPENSE_FETCH_LOADER = "SHOW_EXPENSE_FETCH_LOADER";
export const HIDE_EXPENSE_FETCH_LOADER = "HIDE_EXPENSE_FETCH_LOADER";
export const SHOW_EARNING_FETCH_LOADER = "SHOW_EARNING_FETCH_LOADER";
export const HIDE_EARNING_FETCH_LOADER = "HIDE_EARNING_FETCH_LOADER";
export const CHANGE_EXPENSE_COST_SORT_DIRECTION =
  "CHANGE_EXPENSE_COST_SORT_DIRECTION";
export const CHANGE_EXPENSE_CATEGORY_SORT_DIRECTION =
  "CHANGE_EXPENSE_CATEGORY_SORT_DIRECTION";
export const CHANGE_EXPENSE_DATE_SORT_DIRECTION =
  "CHANGE_EXPENSE_DATE_SORT_DIRECTION";
export const CHANGE_EARNING_INCOME_SORT_DIRECTION =
  "CHANGE_EARNING_INCOME_SORT_DIRECTION";
export const CHANGE_EARNING_DATE_SORT_DIRECTION =
  "CHANGE_EARNING_DATE_SORT_DIRECTION";
export const DELETE_EXPENSE = "DELETE_EXPENSE";
export const DELETE_EARNING = "DELETE_EARNING";
export const FETCH_YEARS = "FETCH_YEARS";
export const SET_YEAR = "SET_YEAR";
export const SIGN_UP = "SIGN_UP";
export const LOGIN = "LOGIN";
export const SHOW_LOADER = "SHOW_LOADER";
export const HIDE_LOADER = "HIDE_LOADER";
export const SHOW_ALERT = "SHOW_ALERT";
export const HIDE_ALERT = "HIDE_ALERT";
export const SHOW_DISABLED = "SHOW_DISABLED";
export const HIDE_DISABLED = "HIDE_DISABLED";
export const SET_ACTIVE_ROUTE = "SET_ACTIVE_ROUTE";
