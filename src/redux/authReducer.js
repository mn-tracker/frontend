import {
  AUTH_TYPE,
  SIGNUP_ALERTS,
  REMOVE_SIGNUP_ALERTS,
  LOGIN_ALERTS,
  REMOVE_LOGIN_ALERTS,
} from "./types";

const initialState = { auth: "Sign up", signUpAlerts: [], loginAlerts: [] };

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_TYPE:
      return { ...state, auth: action.payload };
    case SIGNUP_ALERTS:
      return { ...state, signUpAlerts: action.payload };
    case REMOVE_SIGNUP_ALERTS:
      return { ...state, signUpAlerts: [] };
    case LOGIN_ALERTS:
      return { ...state, loginAlerts: action.payload };
    case REMOVE_LOGIN_ALERTS:
      return { ...state, loginAlerts: [] };
    default:
      return state;
  }
};
