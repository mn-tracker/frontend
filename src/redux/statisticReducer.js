import {
  FETCH_PIE_EXPENSE_STATISTIC,
  FETCH_EXPENSE_AND_EARNING_PER_MONTH,
  FETCH_YEARS,
  SET_YEAR,
} from "./types";

const initialState = {
  expenseAndEarningPerMonth: {
    earning: [],
    expense: [],
    years: [],
    selectedYear: null,
  },
};

export const statisticReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PIE_EXPENSE_STATISTIC:
      return { ...state, pieData: action.payload };
    case FETCH_EXPENSE_AND_EARNING_PER_MONTH:
      return { ...state, expenseAndEarningPerMonth: action.payload };
    case FETCH_YEARS:
      return {
        ...state,
        years: action.payload,
        selectedYear: action.payload.length > 0 && action.payload[0],
      };
    case SET_YEAR:
      return {
        ...state,
        selectedYear: action.payload,
      };
    default:
      return state;
  }
};
