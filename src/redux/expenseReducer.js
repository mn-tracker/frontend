import {
  FETCH_EXPENSES,
  FETCH_CATEGORIES,
  ADD_EXPENSE,
  SHOW_EXPENSE_SUBMIT_LOADER,
  HIDE_EXPENSE_SUBMIT_LOADER,
  SHOW_EXPENSE_FETCH_LOADER,
  HIDE_EXPENSE_FETCH_LOADER,
  DELETE_EXPENSE,
} from "./types";

const initialState = {
  categories: [],
  expenses: { content: [] },
  submitLoading: false,
  fetchLoading: false,
};

export const expenseReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EXPENSES:
      return { ...state, expenses: action.payload };
    case FETCH_CATEGORIES:
      return { ...state, categories: action.payload };
    case ADD_EXPENSE:
      return { ...state, newExpense: action.payload };
    case SHOW_EXPENSE_SUBMIT_LOADER:
      return { ...state, submitLoading: true };
    case HIDE_EXPENSE_SUBMIT_LOADER:
      return { ...state, submitLoading: false };
    case SHOW_EXPENSE_FETCH_LOADER:
      return { ...state, fetchLoading: true };
    case HIDE_EXPENSE_FETCH_LOADER:
      return { ...state, fetchLoading: false };
    case DELETE_EXPENSE: {
      var newContent = state.expenses.content;

      //delete expense by id from redux store
      for (var i = 0; i < newContent.length; i++) {
        if (newContent[i].id === action.payload) newContent.splice(i, 1);
      }

      return { ...state, ...state.expenses, content: newContent };
    }
    default:
      return state;
  }
};
