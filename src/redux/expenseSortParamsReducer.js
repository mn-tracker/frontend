import {
  CHANGE_EXPENSE_COST_SORT_DIRECTION,
  CHANGE_EXPENSE_CATEGORY_SORT_DIRECTION,
  CHANGE_EXPENSE_DATE_SORT_DIRECTION,
} from "./types";

const initialState = {
  costSort: null,
  costSortParam: "",
  categorySort: null,
  categorySortParam: "",
  dateSort: null,
  dateSortParam: "",
};

export const expenseSortParamsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_EXPENSE_COST_SORT_DIRECTION:
      if (state.costSort === "asc") {
        return { ...state, costSort: "desc", costSortParam: "&sort=cost,desc" };
      }
      if (state.costSort === "desc") {
        return { ...state, costSort: null, costSortParam: "" };
      }
      return { ...state, costSort: "asc", costSortParam: "&sort=cost,asc" };

    case CHANGE_EXPENSE_CATEGORY_SORT_DIRECTION:
      if (state.categorySort === "asc") {
        return {
          ...state,
          categorySort: "desc",
          categorySortParam: "&sort=category,desc",
        };
      }
      if (state.categorySort === "desc") {
        return { ...state, categorySort: null, categorySortParam: "" };
      }
      return {
        ...state,
        categorySort: "asc",
        categorySortParam: "&sort=category,asc",
      };

    case CHANGE_EXPENSE_DATE_SORT_DIRECTION:
      if (state.dateSort === "asc") {
        return { ...state, dateSort: "desc", dateSortParam: "&sort=date,desc" };
      }
      if (state.dateSort === "desc") {
        return { ...state, dateSort: null, dateSortParam: "" };
      }
      return { ...state, dateSort: "asc", dateSortParam: "&sort=date,asc" };

    default:
      return state;
  }
};
