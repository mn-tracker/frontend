import {
  SHOW_LOADER,
  HIDE_LOADER,
  SHOW_DISABLED,
  HIDE_DISABLED,
  LOGIN,
  SET_LOGGED,
  REMOVE_LOGGED,
  SET_PROFIT_VALUE,
  SET_ACTIVE_ROUTE,
} from "./types";
import { getCookie } from "../util/CookieManager";

const initilState = {
  loading: true,
  logged: getCookie("access_token") != null,
  profit: 0,
  activeRoute: "",
};

export const appReducer = (state = initilState, action) => {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loading: true };
    case HIDE_LOADER:
      return { ...state, loading: false };
    case SHOW_DISABLED:
      return { ...state, disabled: true };
    case HIDE_DISABLED:
      return { ...state, disabled: false };
    case LOGIN:
      return { ...state };
    case SET_LOGGED:
      return { ...state, logged: true };
    case REMOVE_LOGGED: {
      window.location.href = "/";
      return { ...state, logged: false };
    }
    case SET_PROFIT_VALUE:
      return { ...state, profit: action.payload };
    case SET_ACTIVE_ROUTE:
      return { ...state, activeRoute: action.payload };
    default:
      return state;
  }
};
