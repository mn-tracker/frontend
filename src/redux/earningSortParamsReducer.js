import {
  CHANGE_EARNING_INCOME_SORT_DIRECTION,
  CHANGE_EARNING_DATE_SORT_DIRECTION,
} from "./types";

const initialState = {
  incomeSort: null,
  incomeSortParam: "",
  dateSort: null,
  dateSortParam: "",
};

export const earningSortParamsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_EARNING_INCOME_SORT_DIRECTION:
      if (state.incomeSort === "asc") {
        return {
          ...state,
          incomeSort: "desc",
          incomeSortParam: "&sort=income,desc",
        };
      }
      if (state.incomeSort === "desc") {
        return { ...state, incomeSort: null, incomeSortParam: "" };
      }
      return {
        ...state,
        incomeSort: "asc",
        incomeSortParam: "&sort=income,asc",
      };

    case CHANGE_EARNING_DATE_SORT_DIRECTION:
      if (state.dateSort === "asc") {
        return { ...state, dateSort: "desc", dateSortParam: "&sort=date,desc" };
      }
      if (state.dateSort === "desc") {
        return { ...state, dateSort: null, dateSortParam: "" };
      }
      return { ...state, dateSort: "asc", dateSortParam: "&sort=date,asc" };

    default:
      return state;
  }
};
