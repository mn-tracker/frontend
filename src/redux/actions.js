import {
  SHOW_LOADER,
  HIDE_LOADER,
  SHOW_DISABLED,
  HIDE_DISABLED,
  FETCH_EXPENSES,
  FETCH_EARNINGS,
  AUTH_TYPE,
  SIGNUP_ALERTS,
  REMOVE_SIGNUP_ALERTS,
  LOGIN_ALERTS,
  REMOVE_LOGIN_ALERTS,
  REMOVE_LOGGED,
  FETCH_CATEGORIES,
  ADD_EXPENSE,
  FETCH_PIE_EXPENSE_STATISTIC,
  ADD_EARNING,
  SET_PROFIT_VALUE,
  SHOW_EXPENSE_SUBMIT_LOADER,
  HIDE_EXPENSE_SUBMIT_LOADER,
  SHOW_EARNING_SUBMIT_LOADER,
  HIDE_EARNING_SUBMIT_LOADER,
  SHOW_EXPENSE_FETCH_LOADER,
  HIDE_EXPENSE_FETCH_LOADER,
  SHOW_EARNING_FETCH_LOADER,
  HIDE_EARNING_FETCH_LOADER,
  CHANGE_EXPENSE_COST_SORT_DIRECTION,
  CHANGE_EXPENSE_CATEGORY_SORT_DIRECTION,
  CHANGE_EXPENSE_DATE_SORT_DIRECTION,
  CHANGE_EARNING_INCOME_SORT_DIRECTION,
  CHANGE_EARNING_DATE_SORT_DIRECTION,
  FETCH_EXPENSE_AND_EARNING_PER_MONTH,
  DELETE_EXPENSE,
  DELETE_EARNING,
  FETCH_YEARS,
  SET_YEAR,
  SET_ACTIVE_ROUTE,
} from "./types";
import axios from "axios";
import { API_URL } from "../util/Constants";
import { setCookie, deleteToken, getCookie } from "../util/CookieManager";

export function showLoader() {
  return {
    type: SHOW_LOADER,
  };
}

export function hideLoader() {
  return {
    type: HIDE_LOADER,
  };
}

export function changeAuthType(type) {
  return (dispatch) => {
    dispatch({ type: AUTH_TYPE, payload: type });
  };
}

export function showDisabled() {
  return {
    type: SHOW_DISABLED,
  };
}

export function hideDisabled() {
  return {
    type: HIDE_DISABLED,
  };
}

export function showSignupAlerts(errors) {
  var errs = errors.map((err) => {
    return err.message;
  });

  return (dispatch) => {
    dispatch({ type: SIGNUP_ALERTS, payload: errs });
    setTimeout(() => {
      dispatch(removeSignupAlerts());
    }, 6000);
  };
}

export function removeSignupAlerts() {
  return {
    type: REMOVE_SIGNUP_ALERTS,
  };
}

export function showLoginAlerts(errors) {
  return (dispatch) => {
    dispatch({ type: LOGIN_ALERTS, payload: errors });
    setTimeout(() => {
      dispatch(removeLoginAlerts());
    }, 6000);
  };
}

export function removeLoginAlerts() {
  return {
    type: REMOVE_LOGIN_ALERTS,
  };
}

export function signUp(user) {
  return async (dispatch) => {
    try {
      dispatch(showDisabled());
      const response = await axios.post(API_URL + "users/sign-up", user, {
        timeout: 10000,
      });
      await response.data;
      dispatch(hideDisabled());
      if (response !== null && response.status === 201) {
        dispatch(changeAuthType("Sign in"));
      } else {
        dispatch(showSignupAlerts(response.data));
      }
    } catch (e) {
      console.log(e);
    }
  };
}

export function login(credentials) {
  return async (dispatch) => {
    try {
      dispatch(showDisabled());
      const response = await axios.post(API_URL + "login", credentials);
      const authorization = await response.data.Authorization;
      setCookie("access_token", authorization);

      const currencyResponse = await axios.get(API_URL + "users/currency", {
        headers: { Authorization: authorization },
      });

      const currency = await currencyResponse.data;

      localStorage.setItem("currency", currency);

      dispatch(hideDisabled());
      window.location.href = "/";
    } catch (e) {
      dispatch(hideDisabled());
      dispatch(showLoginAlerts(["Bad credentials"]));
    }
  };
}

export function logOut() {
  deleteToken();
  localStorage.removeItem("currency");
  localStorage.removeItem("chartPerMonthStyle");

  return (dispatch) => {
    dispatch({ type: REMOVE_LOGGED });
  };
}

export function fetchCategories() {
  return async (dispatch) => {
    try {
      const response = await axios.get(API_URL + "categories", {
        headers: { Authorization: getCookie("access_token") },
      });
      const json = await response.data;
      dispatch({ type: FETCH_CATEGORIES, payload: json });
    } catch (e) {
      console.log(e);
    }
  };
}

export function addExpense(expense) {
  return async (dispatch) => {
    try {
      dispatch(showExpenseSubmitLoader());
      dispatch({ type: FETCH_EXPENSES, payload: {} });
      const response = await axios.post(API_URL + "expenses", expense, {
        headers: { Authorization: getCookie("access_token") },
      });
      await response.data;
      dispatch({ type: ADD_EXPENSE, payload: expense });
      dispatch(hideExpenseSubmitLoader());
      dispatch(fetchFullProfit());
      dispatch(fetchYears());
      dispatch(fetchExpenses("expenses"));
    } catch (e) {
      console.log(e);
    }
  };
}

export function addEarning(earning) {
  return async (dispatch) => {
    try {
      dispatch(showEarningSubmitLoader());
      const response = await axios.post(API_URL + "earnings", earning, {
        headers: { Authorization: getCookie("access_token") },
      });
      await response.data;
      dispatch({ type: ADD_EARNING });
      dispatch(hideEarningSubmitLoader());
      dispatch(fetchFullProfit());
      dispatch(fetchYears());
      dispatch(fetchEarnings("earnings"));
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchExpenses(url, showLoader) {
  return async (dispatch) => {
    try {
      if (showLoader) dispatch(showExpenseFetchLoader());

      const response = await axios.get(API_URL + url, {
        headers: { Authorization: getCookie("access_token") },
      });
      const json = await response.data;

      dispatch({ type: FETCH_EXPENSES, payload: json });

      dispatch(hideExpenseFetchLoader());
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchEarnings(url, showLoader) {
  return async (dispatch) => {
    try {
      if (showLoader) dispatch(showEarningFetchLoader());
      console.log(url);
      const response = await axios.get(API_URL + url, {
        headers: { Authorization: getCookie("access_token") },
      });
      const json = await response.data;

      dispatch({ type: FETCH_EARNINGS, payload: json });

      dispatch(hideEarningFetchLoader());
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchPieExpenseStatistics(url) {
  return async (dispatch) => {
    try {
      const response = await axios.get(API_URL + url, {
        headers: { Authorization: getCookie("access_token") },
      });
      const json = await response.data;

      var categories = [];
      var costs = [];

      json.map((stat) => {
        categories.push(stat.categoryName);
        costs.push(stat.cost);
      });

      dispatch({
        type: FETCH_PIE_EXPENSE_STATISTIC,
        payload: { categories: categories, costs: costs },
      });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchFullProfit() {
  return async (dispatch) => {
    try {
      const response = await axios.get(API_URL + "earnings/profit", {
        headers: { Authorization: getCookie("access_token") },
      });
      var profit = await response.data;
      dispatch(setProfitValue(profit !== "" ? profit : 0));
    } catch (e) {
      console.log(e);
    }
  };
}

export function setProfitValue(value) {
  return { type: SET_PROFIT_VALUE, payload: value };
}

export function showExpenseSubmitLoader() {
  return { type: SHOW_EXPENSE_SUBMIT_LOADER };
}
export function hideExpenseSubmitLoader() {
  return { type: HIDE_EXPENSE_SUBMIT_LOADER };
}
export function showEarningSubmitLoader() {
  return { type: SHOW_EARNING_SUBMIT_LOADER };
}
export function hideEarningSubmitLoader() {
  return { type: HIDE_EARNING_SUBMIT_LOADER };
}

export function showExpenseFetchLoader() {
  return { type: SHOW_EXPENSE_FETCH_LOADER };
}
export function hideExpenseFetchLoader() {
  return { type: HIDE_EXPENSE_FETCH_LOADER };
}
export function showEarningFetchLoader() {
  return { type: SHOW_EARNING_FETCH_LOADER };
}
export function hideEarningFetchLoader() {
  return { type: HIDE_EARNING_FETCH_LOADER };
}

export function changeExpenseCostDirection() {
  return { type: CHANGE_EXPENSE_COST_SORT_DIRECTION };
}
export function changeExpenseCategoryDirection() {
  return { type: CHANGE_EXPENSE_CATEGORY_SORT_DIRECTION };
}
export function changeExpenseDateDirection() {
  return { type: CHANGE_EXPENSE_DATE_SORT_DIRECTION };
}

export function changeEarningIncomeDirection() {
  return { type: CHANGE_EARNING_INCOME_SORT_DIRECTION };
}
export function changeEarningDateDirection() {
  return { type: CHANGE_EARNING_DATE_SORT_DIRECTION };
}

export function fetchExpenseAndEarningPerMonth(year) {
  return async (dispatch) => {
    try {
      const expenseResponse = await axios.get(
        API_URL + "expenses/per_month?year=" + year,
        {
          headers: { Authorization: getCookie("access_token") },
        }
      );
      const expenseJson = await expenseResponse.data;
      const earningResponse = await axios.get(
        API_URL + "earnings/per_month?year=" + year,
        {
          headers: { Authorization: getCookie("access_token") },
        }
      );
      const earningJson = await earningResponse.data;

      var months = [];

      expenseJson.map((expense) => {
        months.push(expense.month);
      });

      earningJson.map((earning) => {
        if (months.indexOf(earning.month) === -1) months.push(earning.month);
      });

      for (let i = 0; i < months.length; i++) {
        if (!expenseJson.some((expense) => expense.month === months[i]))
          expenseJson.push({ month: months[i], cost: 0 });

        if (!earningJson.some((earning) => earning.month === months[i]))
          earningJson.push({ month: months[i], income: 0 });
      }

      expenseJson.sort(function (a, b) {
        if (a.month < b.month) return -1;

        if (a.month > b.month) return 1;

        return 0;
      });

      earningJson.sort(function (a, b) {
        if (a.month < b.month) return -1;

        if (a.month > b.month) return 1;

        return 0;
      });

      var monthsName = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      expenseJson.map((expense) => {
        expense.month = monthsName[expense.month - 1];
      });

      earningJson.map((earning) => {
        earning.month = monthsName[earning.month - 1];
      });

      dispatch({
        type: FETCH_EXPENSE_AND_EARNING_PER_MONTH,
        payload: { earning: earningJson, expense: expenseJson },
      });
    } catch (e) {
      console.log(e);
    }
  };
}

export function deleteExpense(expenseId) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_EXPENSE, payload: expenseId });
      const response = await axios.delete(API_URL + "expenses/" + expenseId, {
        headers: { Authorization: getCookie("access_token") },
      });
      await response.data;

      dispatch(fetchFullProfit());
      dispatch(fetchYears());
      dispatch({
        type: FETCH_EXPENSE_AND_EARNING_PER_MONTH,
        payload: { earning: [], expense: [] },
      });
    } catch (e) {
      console.log(e);
    }
  };
}

export function deleteEarning(earningId) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_EARNING, payload: earningId });
      const response = await axios.delete(API_URL + "earnings/" + earningId, {
        headers: { Authorization: getCookie("access_token") },
      });
      await response.data;

      dispatch(fetchFullProfit());
      dispatch(fetchYears());
      dispatch({
        type: FETCH_EXPENSE_AND_EARNING_PER_MONTH,
        payload: { earning: [], expense: [] },
      });
    } catch (e) {
      console.log(e);
    }
  };
}

export function fetchYears() {
  return async (dispatch) => {
    try {
      const response = await axios.get(API_URL + "expenses/years", {
        headers: { Authorization: getCookie("access_token") },
      });
      const json = await response.data;

      dispatch({ type: FETCH_YEARS, payload: json });
      if (json.length > 0) dispatch(setYear(json[0]));
    } catch (e) {
      console.log(e);
    }
  };
}

export function setYear(year) {
  return async (dispatch) => {
    dispatch(fetchExpenseAndEarningPerMonth(year));
    dispatch({ type: SET_YEAR, payload: year });
  };
}

export function setActiveRoute(route) {
  return { type: SET_ACTIVE_ROUTE, payload: route };
}
