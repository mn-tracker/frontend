import {
  FETCH_EARNINGS,
  ADD_EARNING,
  SHOW_EARNING_SUBMIT_LOADER,
  HIDE_EARNING_SUBMIT_LOADER,
  SHOW_EARNING_FETCH_LOADER,
  HIDE_EARNING_FETCH_LOADER,
  DELETE_EARNING,
} from "./types";

const initialState = {
  earnings: { content: [] },
  submitLoading: false,
  fetchLoading: false,
};

export const earningReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EARNINGS:
      return { ...state, earnings: action.payload };
    case ADD_EARNING:
      return state;
    case SHOW_EARNING_SUBMIT_LOADER:
      return { ...state, submitLoading: true };
    case HIDE_EARNING_SUBMIT_LOADER:
      return { ...state, submitLoading: false };
    case SHOW_EARNING_FETCH_LOADER:
      return { ...state, fetchLoading: true };
    case HIDE_EARNING_FETCH_LOADER:
      return { ...state, fetchLoading: false };
    case DELETE_EARNING: {
      var newContent = state.earnings.content;

      //delete expense by id from redux store
      for (var i = 0; i < newContent.length; i++) {
        if (newContent[i].id === action.payload) newContent.splice(i, 1);
      }

      return { ...state, ...state.earnings, content: newContent };
    }
    default:
      return state;
  }
};
