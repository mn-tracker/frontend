import React from "react";

export default () => {
  let card = [];
  for (let i = 0; i < 3; i++) {
    card.push(
      <div className="spinner-grow text-primary" role="status" key={i}>
        <span className="sr-only">Loading...</span>
      </div>
    );
  }

  return <div className="d-flex justify-content-center">{card}</div>;
};
