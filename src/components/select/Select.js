import React from "react";

export default (props) => {
  return (
    <select
      id={props.id}
      required={props.required}
      name={props.name}
      className={"custom-select " + props.classes}
      onChange={props.onChange}
    >
      {props.defaultSelected && (
        <option selected disabled value="">
          {props.defaultSelected}
        </option>
      )}

      {props.options.map((option) => {
        return (
          <option
            selected={props.selected === option}
            value={option}
            key={option}
          >
            {option}
          </option>
        );
      })}
    </select>
  );
};
