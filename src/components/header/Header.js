import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./headerStyle.css";
import { connect } from "react-redux";
import { logOut, changeAuthType } from "../../redux/actions";
import $ from "jquery";
import { setProfit } from "../../util/Util";

class Header extends Component {
  componentDidMount() {
    $(document).ready(function () {
      $("#navbar-toggler").bind("swiperight", function (e, touch) {
        $('[data-toggle="collapse"]')[0].click();
      });

      //hide navbar menu on clicking outside
      $(".content.container").click(function () {
        if ($(".navbar-collapse").hasClass("show")) {
          $(".navbar-collapse").removeClass("show").removeClass("collapse");
          $(".navbar-collapse").addClass("collapsing");
          setTimeout(() => {
            $(".navbar-collapse")
              .removeClass("collapsing")
              .addClass("collapse");
          }, 100);
        }
      });

      $(".nav-link").click(function () {
        if (
          $(".navbar-collapse").hasClass("show") &&
          !$(this).hasClass("dropdown-toggle")
        ) {
          $(".navbar-collapse").removeClass("show").removeClass("collapse");
          $(".navbar-collapse").addClass("collapsing");
          setTimeout(() => {
            $(".navbar-collapse")
              .removeClass("collapsing")
              .addClass("collapse");
          }, 100);
        }
      });

      $(".navbar-brand").click(function () {
        if (
          $(".navbar-collapse").hasClass("show") &&
          !$(this).hasClass("dropdown-toggle")
        ) {
          $(".navbar-collapse").removeClass("show").removeClass("collapse");
          $(".navbar-collapse").addClass("collapsing");
          setTimeout(() => {
            $(".navbar-collapse")
              .removeClass("collapsing")
              .addClass("collapse");
          }, 100);
        }
      });

      $(".dropdown-item").click(function () {
        if ($(".navbar-collapse").hasClass("show")) {
          $(".navbar-collapse").removeClass("show").removeClass("collapse");
          $(".navbar-collapse").addClass("collapsing");
          setTimeout(() => {
            $(".navbar-collapse")
              .removeClass("collapsing")
              .addClass("collapse");
          }, 100);
        }
      });
    });
  }

  render() {
    const profit = setProfit(this.props.state.app.profit);

    return (
      <div className="header">
        <nav className="navbar fixed-top navbar-expand-xl">
          <Link className="navbar-brand" to="/">
            <img src="/logo.png" width="45px" alt="Home" title="Home" />
          </Link>
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link
                className={"nav-link profit-link " + this.isActive("profit")}
                to="/profit"
              >
                Profit&nbsp;
                <span
                  className={
                    profit.profitClass !== null
                      ? "badge badge-light " + profit.profitClass
                      : "badge badge-light "
                  }
                >
                  {profit.profitValue}
                </span>
              </Link>
            </li>
          </ul>
          <button
            id="navbar-toggler"
            className="navbar-toggler p-0"
            type="button"
            data-toggle="collapse"
            data-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="fas fa-bars"></i>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className={"nav-link " + this.isActive("home")} to="/">
                  <i className="fas fa-home"></i>&nbsp; Home
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={"nav-link " + this.isActive("expenses")}
                  to="/expenses"
                >
                  <i className="fas fa-long-arrow-alt-down"></i>&nbsp; Expenses
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={"nav-link " + this.isActive("earnings")}
                  to="/earnings"
                >
                  <i className="fas fa-long-arrow-alt-up"></i>&nbsp; Earnings
                </Link>
              </li>
              <li className="nav-item dropdown">
                <span
                  className="nav-link dropdown-toggle"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i className="fas fa-user-circle" />
                  &nbsp;Account
                </span>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <span
                    className="dropdown-item"
                    data-toggle="modal"
                    data-target="#staticBackdrop"
                    onClick={() => {
                      this.props.changeAuthType("Sign up");
                    }}
                  >
                    Sign-up
                  </span>
                  <div className="dropdown-divider"></div>
                  {this.props.state.app.logged ? (
                    <span
                      className="dropdown-item"
                      onClick={() => this.props.logOut()}
                    >
                      Log out
                    </span>
                  ) : (
                    <span
                      className="dropdown-item"
                      data-toggle="modal"
                      id="sign-in-trigger"
                      data-target="#staticBackdrop"
                      onClick={() => {
                        this.props.changeAuthType("Sign in");
                      }}
                    >
                      Login
                    </span>
                  )}
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }

  isActive(routeName) {
    return routeName === this.props.state.app.activeRoute ? "active" : "";
  }
}

const mapDispatchToProps = {
  logOut,
  changeAuthType,
};

const mapStateToProps = (state) => ({
  state,
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
