import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchExpenses,
  changeExpenseCostDirection,
  changeExpenseCategoryDirection,
  changeExpenseDateDirection,
  setActiveRoute,
} from "../../redux/actions";
import Loader from "../loader/Loader";
import { Link } from "react-router-dom";
import $ from "jquery";
import Pagination from "react-js-pagination";
import DateRangeFilter from "../date/DateRangeFilter";
import * as moment from "moment";
import ActionsTable from "../table/ActionsTable";
import { getDateFilter, setDateFilter } from "../../util/Util";
import "../../css/pagination.css";

class Expenses extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activePage: 0,
      dateStartParam: "",
      dateEndParam: "",
      dateStart: null,
      dateEnd: null,
    };
  }

  getExpenses(showLoader) {
    this.props.fetchExpenses(
      "expenses?page=" +
        this.state.activePage +
        this.state.dateStartParam +
        this.state.dateEndParam +
        this.props.expenseSortParams.costSortParam +
        this.props.expenseSortParams.categorySortParam +
        this.props.expenseSortParams.dateSortParam,
      showLoader
    );
  }

  componentDidMount() {
    this.props.setActiveRoute("expenses");
    getDateFilter(
      "expenses",
      this.setState.bind(this),
      this.getExpenses.bind(this),
      ["&", "&"]
    );

    $(document).ready(function () {
      $(".block-title").click(function (event) {
        $(this).toggleClass("block-active").next().slideToggle(150);
      });
    });
  }

  handlePageChange(pageNumber) {
    if (pageNumber !== this.state.activePage + 1) {
      this.setState({ activePage: pageNumber - 1 }, function () {
        this.getExpenses(false);
      });
    }
  }

  dateStartInputHandler = (e) => {
    const newDate = moment(e, "DD-MM-YYYY").format();

    this.setState({
      dateStart: e,
      dateStartParam: "&dateStart=" + newDate.split("T")[0],
    });
  };

  dateEndInputHandler = (e) => {
    const newDate = moment(e, "DD-MM-YYYY").format();
    this.setState({
      dateEnd: e,
      dateEndParam: "&dateEnd=" + newDate.split("T")[0],
    });
  };

  submitHandler = (e) => {
    e.preventDefault();
    setDateFilter(
      "expenses",
      this.state.dateStart,
      this.state.dateEnd,
      this.getExpenses.bind(this)
    );
  };

  clearDateInputs() {
    localStorage.removeItem("expensesDateFilter");
    this.setState(
      {
        dateStartParam: "",
        dateEndParam: "",
        dateStart: null,
        dateEnd: null,
      },
      function () {
        this.getExpenses(true);
      }
    );
  }

  changeCostDirection() {
    this.props.changeExpenseCostDirection();
    setTimeout(() => {
      this.getExpenses();
    }, 10);
  }

  changeCategoryDirection() {
    this.props.changeExpenseCategoryDirection();
    setTimeout(() => {
      this.getExpenses();
    }, 10);
  }

  changeDateDirection() {
    this.props.changeExpenseDateDirection();
    setTimeout(() => {
      this.getExpenses();
    }, 10);
  }

  render() {
    const expenses = this.props.expenses.expenses;
    return (
      <div className="expenses container">
        <div className="row justify-content-center">
          <div className="col">
            <Link to="/expenses/add">
              <button type="button" className="btn btn-primary">
                Add expense
              </button>
            </Link>
          </div>
        </div>
        <DateRangeFilter
          submitHandler={this.submitHandler}
          dateStart={this.state.dateStart}
          dateEnd={this.state.dateEnd}
          dateStartInputHandler={this.dateStartInputHandler}
          dateEndInputHandler={this.dateEndInputHandler}
          clearDateInputs={this.clearDateInputs.bind(this)}
        />
        <div className="row justify-content-center">
          <div className="col">
            {!this.props.expenses.fetchLoading && !expenses.empty && (
              <ActionsTable
                name="expenses"
                actions={expenses.content}
                colNames={[
                  {
                    colName: "Category",
                    sortFunction: this.changeCategoryDirection.bind(this),
                    sortDirection: this.props.expenseSortParams.categorySort,
                    colValueName: "category",
                  },
                  {
                    colName: "Expense",
                    sortFunction: this.changeCostDirection.bind(this),
                    sortDirection: this.props.expenseSortParams.costSort,
                    colValueName: "cost",
                  },
                  {
                    colName: "Date",
                    sortFunction: this.changeDateDirection.bind(this),
                    sortDirection: this.props.expenseSortParams.dateSort,
                    colValueName: "date",
                  },
                ]}
                classes="expense-table"
              />
            )}
            {this.props.expenses.fetchLoading && <Loader />}
          </div>
        </div>
        <div className="row justify-content-center">
          {!this.props.expenses.fetchLoading &&
            expenses.size &&
            expenses.content.length > 0 && (
              <Pagination
                key={"paginationList"}
                activePage={this.state.activePage + 1}
                itemsCountPerPage={expenses.size}
                totalItemsCount={expenses.totalElements}
                pageRangeDisplayed={3}
                nextPageText="❯"
                prevPageText="❮"
                itemClassPrev="prev-next-arrows"
                itemClassNext="prev-next-arrows"
                onChange={this.handlePageChange.bind(this)}
              />
            )}
          {!this.props.expenses.fetchLoading &&
            expenses.content.length === 0 &&
            "Empty data"}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  fetchExpenses,
  changeExpenseCostDirection,
  changeExpenseCategoryDirection,
  changeExpenseDateDirection,
  setActiveRoute,
};

const mapStateToProps = (state) => ({
  expenseSortParams: state.expenseSortParams,
  expenses: state.expenses,
});

export default connect(mapStateToProps, mapDispatchToProps)(Expenses);
