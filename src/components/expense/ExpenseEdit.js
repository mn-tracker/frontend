import React, { Component } from "react";
import { connect } from "react-redux";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { fetchCategories, addExpense } from "../../redux/actions";
import { isMobile } from "react-device-detect";
import * as moment from "moment";
import "../../css/addForm.css";
import Select from "../select/Select";

class ExpenseEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      date: null,
      category: "",
      cost: null,
    };
  }

  componentDidMount() {
    const expense = this.props.location.action;
    if (expense != null) {
      this.setState({
        id: expense.id,
        date: moment(expense.date).toDate(),
        category: expense.category,
        cost: expense.cost,
      });
      this.props.fetchCategories();
    } else this.props.history.push("/expenses");
  }

  submitHandler = (e) => {
    e.preventDefault();
    this.props.addExpense(this.state);
  };

  inputChangeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  dateInputHandler(date) {
    this.setState({ date: date });
  }

  render() {
    const disabled = this.props.state.expenses.submitLoading;

    return (
      <form onSubmit={this.submitHandler}>
        <div className="form-wrapper">
          <div className="row justify-content-center">
            <label className="date-label">Date</label>
          </div>
          <div className="row justify-content-center">
            <div className="col datetime-col">
              <DatePicker
                selected={this.state.date}
                onChange={(date) => {
                  this.dateInputHandler(date);
                }}
                withPortal={isMobile}
                className="datetime-picker-input"
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={15}
                timeCaption="time"
                dateFormat="MM/dd/yyyy HH:mm"
                popperPlacement="bottom"
                popperModifiers={{
                  flip: {
                    behavior: ["bottom"], // don't allow it to flip to be above
                  },
                  preventOverflow: {
                    enabled: false, // tell it not to try to stay within the view (this prevents the popper from covering the element you clicked)
                  },
                  hide: {
                    enabled: false, // turn off since needs preventOverflow to be enabled
                  },
                }}
              />
            </div>
          </div>
          <div className="row justify-content-center py-3">
            <input
              required
              type="text"
              pattern="[0-9]*"
              id="cost-input"
              className="form-control decimal-input"
              placeholder="Cost"
              name="cost"
              value={this.state.cost}
              onChange={this.inputChangeHandler}
            />
          </div>

          <div className="row justify-content-center">
            <Select
              required={true}
              name="category"
              onChange={this.inputChangeHandler}
              options={this.props.state.expenses.categories}
              defaultSelected="Category"
              selected={this.state.category}
            />
          </div>

          <div className="row justify-content-md-center py-2">
            <button
              disabled={disabled}
              type="submit"
              className="submit-btn btn btn-primary"
            >
              {disabled && <i className="fas fa-circle-notch fa-spin"></i>}
              {disabled && " "}
              Submit
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapDispatchToProps = {
  addExpense,
  fetchCategories,
};

const mapStateToProps = (state) => ({
  state,
});

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseEdit);
