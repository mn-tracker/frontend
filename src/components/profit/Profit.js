import React, { Component } from "react";
import { Line } from "react-chartjs-2";
import { connect } from "react-redux";
import { setProfit } from "../../util/Util";
import Select from "../select/Select";
import {
  fetchExpenseAndEarningPerMonth,
  fetchYears,
  setActiveRoute,
} from "../../redux/actions";
import "./profit.css";

class Profit extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.setActiveRoute("profit");
    this.props.fetchYears();
  }

  selectChangeHandler = (e) => {
    localStorage.setItem("chartPerMonthStyle", JSON.stringify(e.target.value));
    this.setState({ chartPerMonthStyle: e.target.value });
  };

  yearSelectChangeHandler = (e) => {
    localStorage.setItem("statsYear", JSON.stringify(e.target.value));
    this.props.fetchExpenseAndEarningPerMonth(e.target.value);
  };

  render() {
    const expenses = this.props.state.statistics.expenseAndEarningPerMonth
      .expense;
    const earnings = this.props.state.statistics.expenseAndEarningPerMonth
      .earning;

    const months = earnings.map((earning) => {
      return earning.month;
    });

    const profitValues = earnings.map((earning, i) => {
      return earning.income - expenses[i].cost;
    });

    const expenseLastMonth =
      expenses.length > 0 ? (
        <span className="expense">
          {expenses[expenses.length - 1].cost}&nbsp;
          <i className="fas fa-long-arrow-alt-down"></i>
        </span>
      ) : (
        <i className="fas fa-ellipsis-h fa-red faa-pulse animated"></i>
      );

    const earningLastMonth =
      earnings.length > 0 ? (
        <span className="earning">
          {earnings[earnings.length - 1].income}&nbsp;
          <i className="fas fa-long-arrow-alt-up"></i>
        </span>
      ) : (
        <i className="fas fa-ellipsis-h fa-green faa-pulse animated"></i>
      );

    const profitLastMonth =
      profitValues.length > 0
        ? setProfit(profitValues[profitValues.length - 1])
        : {
            profitValue: (
              <i className="fas fa-ellipsis-h fa-dark-green faa-pulse animated"></i>
            ),
          };

    return (
      <div className="profit">
        <table className="profit-table">
          <tbody>
            <tr>
              <td>
                <span className="label-text">Expense this month :</span>
              </td>
              <td>{expenseLastMonth}</td>
            </tr>
            <tr>
              <td>
                <span className="label-text">Earning this month :</span>
              </td>
              <td>{earningLastMonth}</td>
            </tr>
            <tr>
              <td>
                <span className="label-text">Profit this month :</span>
              </td>
              <td>
                <span className={profitLastMonth.profitClass}>
                  {profitLastMonth.profitValue}
                </span>
              </td>
            </tr>
          </tbody>
        </table>

        <br />
        {this.props.state.statistics.selectedYear != null && (
          <div className="chart-wrapper">
            Year
            <Select
              id="chart-year-select"
              classes="chart-stats"
              onChange={this.yearSelectChangeHandler}
              options={this.props.state.statistics.years}
              selected={this.props.state.statistics.selectedYear}
            />
            {months.length > 0 && (
              <Line
                data={{
                  labels: months,
                  datasets: [
                    {
                      label: "Profit",
                      data: profitValues,
                      backgroundColor: "white",
                      fill: false, // Don't fill area under the line
                      borderColor: "orange", // Line color
                    },
                  ],
                }}
                options={{
                  scales: {
                    yAxes: [
                      {
                        display: true,
                        ticks: {
                          beginAtZero: true,
                        },
                      },
                    ],
                    xAxes: [
                      {
                        display: true,
                      },
                    ],
                  },
                }}
                width="200px"
                height="200px"
              />
            )}
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = {
  fetchExpenseAndEarningPerMonth,
  fetchYears,
  setActiveRoute,
};

export default connect(mapStateToProps, mapDispatchToProps)(Profit);
