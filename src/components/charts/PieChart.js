import React from "react";
import { Pie } from "react-chartjs-2";
import { connect } from "react-redux";
import "./charts.css";
import DateRangeFilter from "../date/DateRangeFilter";
import $ from "jquery";
import * as moment from "moment";
import { fetchPieExpenseStatistics } from "../../redux/actions";
import { getDateFilter, setDateFilter } from "../../util/Util";
import Loader from "../loader/Loader";

class PieChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataPie: null,
      dateStart: null,
      dateEnd: null,
      dateStartParam: "",
      dateEndParam: "",
      emptyData: {
        labels: ["Empty"],
        datasets: [
          {
            data: [1],
            backgroundColor: ["#dfffde"],
            hoverBackgroundColor: ["#e6ede6"],
          },
        ],
      },
    };
  }

  getExpensesStatistics() {
    this.props.fetchPieExpenseStatistics(
      "expenses/statistics" +
        this.state.dateStartParam +
        this.state.dateEndParam
    );
  }

  componentDidMount() {
    // fetch data for pie Chart
    getDateFilter(
      "expenses",
      this.setState.bind(this),
      this.getExpensesStatistics.bind(this),
      ["?", "&"]
    );

    $(document).ready(function () {
      $(".block-title").click(function (event) {
        $(this).toggleClass("block-active").next().slideToggle(150);
      });
    });
  }

  dateStartInputHandler = (e) => {
    const newDate = moment(e, "DD-MM-YYYY").format();

    this.setState({
      dateStart: e,
      dateStartParam: "?dateStart=" + newDate.split("T")[0],
    });
  };

  dateEndInputHandler = (e) => {
    const newDate = moment(e, "DD-MM-YYYY").format();
    this.setState({
      dateEnd: e,
      dateEndParam: "&dateEnd=" + newDate.split("T")[0],
    });
  };

  submitHandler = (e) => {
    e.preventDefault();

    setDateFilter(
      "expenses",
      this.state.dateStart,
      this.state.dateEnd,
      this.getExpensesStatistics.bind(this)
    );
  };

  clearDateInputs() {
    localStorage.removeItem("expensesDateFilter");
    if (this.state.dateStart !== null || this.state.dateEnd !== null) {
      this.setState(
        {
          dateStartParam: "",
          dateEndParam: "",
          dateStart: null,
          dateEnd: null,
        },
        function () {
          this.getExpensesStatistics();
        }
      );
    }
  }

  render() {
    return (
      <div className="chart-wrapper">
        <DateRangeFilter
          submitHandler={this.submitHandler}
          dateStart={this.state.dateStart}
          dateEnd={this.state.dateEnd}
          dateStartInputHandler={this.dateStartInputHandler}
          dateEndInputHandler={this.dateEndInputHandler}
          clearDateInputs={this.clearDateInputs.bind(this)}
        />

        {this.props.state.statistics.pieData ? (
          this.props.state.statistics.pieData.categories.length === 0 ? (
            <Pie data={this.state.emptyData} width={200} height={180} />
          ) : (
            <Pie
              data={{
                labels: this.props.state.statistics.pieData.categories,
                datasets: [
                  {
                    data: this.props.state.statistics.pieData.costs,
                    backgroundColor: [
                      "#F7464A",
                      "#46BFBD",
                      "#FDB45C",
                      "#949FB1",
                      "#4D5360",
                      "#AC64AD",
                      "#0074D9",
                      "#DDDDDD",
                      "#2ECC40",
                      "#66532c",
                      "#f8fa70",
                    ],
                    hoverBackgroundColor: [
                      "#FF5A5E",
                      "#5AD3D1",
                      "#FFC870",
                      "#A8B3C5",
                      "#616774",
                      "#DA92DB",
                    ],
                  },
                ],
              }}
              width={200}
              height={220}
            />
          )
        ) : (
          <Loader />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = {
  fetchPieExpenseStatistics,
};

export default connect(mapStateToProps, mapDispatchToProps)(PieChart);
