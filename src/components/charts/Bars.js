import React from "react";
import { Bar } from "react-chartjs-2";

export default ({ data }) => {
  const options = {
    responsive: true,
    legend: {
      display: true,
    },
    scales: {
      yAxes: [
        {
          display: true,
          ticks: {
            beginAtZero: true,
          },
        },
      ],
      xAxes: [
        {
          display: true,
        },
      ],
    },
    type: "bar",
  };

  return (
    <Bar
      data={{
        labels: data.expense.map((expense) => {
          return expense.month;
        }),
        datasets: [
          {
            label: "Expense",
            backgroundColor: "#FF4136",
            // stack: 1,
            hoverBackgroundColor: "#ff6259",
            data: data.expense.map((expense) => {
              return expense.cost;
            }),
          },

          {
            label: "Earning",
            backgroundColor: "#00a80e",
            // stack: 1,
            hoverBackgroundColor: "#01FF70",
            data: data.earning.map((earning) => {
              return earning.income;
            }),
          },
        ],
      }}
      width={200}
      height={200}
      options={options}
    />
  );
};
