import React from "react";
import { Line } from "react-chartjs-2";
import "./charts.css";

export default ({ data }) => {
  const lineData = {
    labels: data.expense.map((expense) => {
      return expense.month;
    }),
    datasets: [
      {
        label: "Expense",
        data: data.expense.map((expense) => {
          return expense.cost;
        }),
        backgroundColor: "white",
        fill: false,
        borderColor: "red",
      },
      {
        label: "Earning",
        data: data.earning.map((earning) => {
          return earning.income;
        }),
        backgroundColor: "white",
        fill: false,
        borderColor: "#00a80e",
      },
    ],
  };
  return (
    <Line
      data={lineData}
      options={{
        scales: {
          yAxes: [
            {
              display: true,
              ticks: {
                beginAtZero: true,
              },
            },
          ],
          xAxes: [
            {
              display: true,
            },
          ],
        },
      }}
      width={200}
      height={200}
    />
  );
};
