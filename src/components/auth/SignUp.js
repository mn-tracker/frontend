import React from "react";
import Alert from "../alert/Alert";
import { useSelector } from "react-redux";

export default ({ inputChangeHandler, submitHandler, state, disabled }) => {
  const alerts = useSelector((state) => state.auth.signUpAlerts);
  return (
    <form onSubmit={submitHandler}>
      {alerts.map((alertText) => {
        return (
          <div className="row justify-content-md-center py-2">
            <Alert text={alertText} />
          </div>
        );
      })}
      <div className="row  ">
        <div>
          <input
            required
            type="text"
            className="form-control"
            placeholder="Login"
            name="login"
            value={state.login}
            onChange={inputChangeHandler}
          />
        </div>
      </div>
      <div className="row py-2">
        <div>
          <input
            required
            type="password"
            className="form-control"
            placeholder="Password"
            name="password"
            value={state.password}
            onChange={inputChangeHandler}
          />
        </div>
      </div>
      <div className="row">
        <div>
          <select
            className="custom-select"
            required
            name="currency"
            onChange={inputChangeHandler}
          >
            <option key="currency1" selected disabled value="">
              Currency
            </option>
            <option key="currency2">$</option>
            <option key="currency3">€</option>
            <option key="currency4">MDL</option>
            <option key="currency5">RON</option>
            <option key="currency6">₽</option>
          </select>
        </div>
      </div>

      <div className="row justify-content-md-center py-2">
        <button
          disabled={disabled}
          type="submit"
          className="submit-btn btn btn-primary"
        >
          {disabled && <i className="fas fa-circle-notch fa-spin"></i>}
          {disabled && " "}
          Submit
        </button>
      </div>
    </form>
  );
};
