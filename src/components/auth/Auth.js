import React, { Component } from "react";
import { connect } from "react-redux";
import { changeAuthType, signUp, login } from "./../../redux/actions";
import SignUp from "./SignUp";
import SignIn from "./SignIn";
import { getCookie } from "../../util/CookieManager";
import * as $ from "jquery";
import "./auth.css";

class Auth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: "IonC07",
      password: "ciolacu.test",
      currency: "",
    };
  }

  inputChangeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  submitHandler = (e) => {
    e.preventDefault();
    if (this.props.state.auth.auth === "Sign up") this.props.signUp(this.state);
    else
      this.props.login({
        login: this.state.login,
        password: this.state.password,
      });
  };

  componentDidMount() {
    if (getCookie("access_token") == null) {
      $(document).ready(function () {
        $("#sign-in-trigger")[0].click();
      });
    }
  }

  render() {
    const authType = this.props.state.auth.auth;

    const switched = authType === "Sign up" ? "Sign in" : "Sign up";

    return (
      <div
        className="modal fade"
        id="staticBackdrop"
        data-backdrop="static"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">
                {this.props.state.auth.auth}
              </h5>
              {getCookie("access_token") == null ? (
                <button type="button" className="close" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              ) : (
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              )}
            </div>
            <div className="modal-body">
              {authType === "Sign up" ? (
                <SignUp
                  inputChangeHandler={this.inputChangeHandler}
                  state={this.state}
                  submitHandler={this.submitHandler}
                  disabled={this.props.state.app.disabled}
                />
              ) : (
                <SignIn
                  inputChangeHandler={this.inputChangeHandler}
                  state={this.state}
                  submitHandler={this.submitHandler}
                  disabled={this.props.state.app.disabled}
                />
              )}
              <span
                className="switch-auth"
                onClick={() => {
                  this.setState(
                    { login: "", password: "", currency: "" },
                    function () {
                      this.props.changeAuthType(switched);
                    }
                  );
                }}
              >
                {switched}
              </span>
            </div>
            <div className="modal-footer">
              {getCookie("access_token") == null ? (
                <button type="button" className="btn btn-secondary">
                  Close
                </button>
              ) : (
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  changeAuthType,
  signUp,
  login,
};

const mapStateToProps = (state) => ({
  state,
});

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
