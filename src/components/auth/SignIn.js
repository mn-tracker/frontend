import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Alert from "../alert/Alert";
export default ({ inputChangeHandler, submitHandler, state, disabled }) => {
  const alerts = useSelector((state) => state.auth.loginAlerts);

  return (
    <form onSubmit={submitHandler}>
      {alerts.map((alertText) => {
        return (
          <div className="row justify-content-md-center py-2">
            <Alert text={alertText} />
          </div>
        );
      })}
      <div className="row  ">
        <div className="col-7">
          <input
            required
            type="text"
            className="form-control"
            placeholder="Login"
            name="login"
            value={state.login}
            onChange={inputChangeHandler}
          />
        </div>
      </div>
      <div className="row py-2">
        <div className="col-7">
          <input
            required
            type="password"
            className="form-control"
            placeholder="Password"
            name="password"
            value={state.password}
            onChange={inputChangeHandler}
          />
        </div>
      </div>

      <div className="row justify-content-md-center py-2">
        <button
          disabled={disabled}
          type="submit"
          className="submit-btn btn btn-primary"
        >
          {disabled && <i className="fas fa-circle-notch fa-spin"></i>}
          {disabled && " "}
          Submit
        </button>
      </div>
    </form>
  );
};
