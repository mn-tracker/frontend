import React from "react";
import PieChart from "../charts/PieChart";
import { connect } from "react-redux";
import { setYear, fetchYears, setActiveRoute } from "../../redux/actions";
import LineChart from "../charts/LineChart";
import Bars from "../charts/Bars";
import "./home.css";
import Select from "../select/Select";
import { setInStorage } from "../../util/Util";

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      chartPerMonthStyle: "Bars",
    };
  }

  componentDidMount() {
    this.props.setActiveRoute("home");
    var chartStyle = JSON.parse(localStorage.getItem("chartPerMonthStyle"));

    if (chartStyle !== null) {
      this.setState({
        chartPerMonthStyle: chartStyle,
      });
    }

    this.props.fetchYears();
  }

  selectChangeHandler = (e) => {
    setInStorage("chartPerMonthStyle", e.target.value);
    this.setState({ chartPerMonthStyle: e.target.value });
  };

  yearSelectChangeHandler = (e) => {
    this.props.setYear(e.target.value);
  };

  render() {
    return (
      <div>
        <br />
        <PieChart />
        <br />
        <br />
        <br />
        {this.props.state.statistics.selectedYear != null && (
          <div className="chart-wrapper">
            <br />
            <div className="chart-stats-select">
              <div className="chart-stats-select-name">
                <label htmlFor="chart-style-select">Chart style</label>
              </div>
              <div className="chart-stats-select-name">
                <label htmlFor="chart-year-select">Year</label>
              </div>
              <br />

              <Select
                id="chart-style-select"
                name="test"
                classes="chart-stats"
                onChange={this.selectChangeHandler}
                options={["Bars", "Lines"]}
                selected={this.state.chartPerMonthStyle}
              />

              <Select
                id="chart-year-select"
                classes="chart-stats"
                onChange={this.yearSelectChangeHandler}
                options={this.props.state.statistics.years}
                selected={this.props.state.statistics.selectedYear}
              />
            </div>
            {this.props.state.statistics.expenseAndEarningPerMonth &&
              (this.state.chartPerMonthStyle !== "Lines" ? (
                <Bars
                  data={this.props.state.statistics.expenseAndEarningPerMonth}
                />
              ) : (
                <LineChart
                  data={this.props.state.statistics.expenseAndEarningPerMonth}
                />
              ))}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = {
  fetchYears,
  setYear,
  setActiveRoute,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
