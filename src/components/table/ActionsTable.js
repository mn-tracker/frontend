import React from "react";
import "./table.css";
import { useDispatch } from "react-redux";
import { deleteExpense, deleteEarning } from "../../redux/actions";
import { Link } from "react-router-dom";

export default ({ name, actions, colNames, classes }) => {
  const dispatch = useDispatch();

  return (
    <table className="table">
      <thead className={`thead ${classes}`}>
        <tr>
          <th scope="col">Edit</th>
          {colNames.map((col) => {
            return (
              <th key={name + "." + col.colName} scope="col">
                <span
                  className="col-name"
                  onClick={() => {
                    if (col.sortFunction) col.sortFunction();
                  }}
                >
                  {col.colName}&nbsp;
                  {getSortIcon(col.sortDirection)}
                </span>
              </th>
            );
          })}
        </tr>
      </thead>
      <tbody>
        {actions.map((action) => {
          return (
            <tr>
              <th scope="row">
                <span
                  className="delete-icon"
                  title="Delete"
                  onClick={() => {
                    if (
                      window.confirm(
                        "Are you sure you wish to delete this action?"
                      )
                    )
                      deleteAction(action.id, dispatch, name);
                  }}
                >
                  <i className="fas fa-trash"></i>
                </span>
                <Link
                  to={{
                    pathname: "/" + name + "/edit",
                    action: action,
                  }}
                >
                  <span className="edit-icon" title="Edit">
                    <i className="fas fa-pen"></i>
                  </span>
                </Link>
              </th>
              {colNames.map((col) => {
                return <td>{action[col.colValueName]}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

const deleteAction = (actionId, dispatch, name) => {
  if (name === "expenses") dispatch(deleteExpense(actionId));
  if (name === "earnings") dispatch(deleteEarning(actionId));
};

const getSortIcon = (sortDirection) => {
  switch (sortDirection) {
    case "asc":
      return <i className="fas fa-caret-up"></i>;
    case "desc":
      return <i className="fas fa-caret-down"></i>;
    default:
      return <i className="fas fa-sort"></i>;
  }
};
