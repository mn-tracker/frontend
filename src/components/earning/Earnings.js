import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchEarnings,
  changeEarningIncomeDirection,
  changeEarningDateDirection,
  setActiveRoute,
} from "../../redux/actions";
import Loader from "../loader/Loader";
import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import ActionsTable from "../table/ActionsTable";
import DateRangeFilter from "../date/DateRangeFilter";
import * as moment from "moment";
import $ from "jquery";
import { setDateFilter, getDateFilter } from "../../util/Util";
import "../../css/pagination.css";

class Earnings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activePage: 0,
      dateStartParam: "",
      dateEndParam: "",
      dateStart: null,
      dateEnd: null,
    };
  }

  getEarnings(showLoader) {
    this.props.fetchEarnings(
      "earnings?page=" +
        this.state.activePage +
        this.state.dateStartParam +
        this.state.dateEndParam +
        this.props.earningSortParams.incomeSortParam +
        this.props.earningSortParams.dateSortParam,
      showLoader
    );
  }

  componentDidMount() {
    this.props.setActiveRoute("earnings");
    getDateFilter(
      "earnings",
      this.setState.bind(this),
      this.getEarnings.bind(this),
      ["&", "&"]
    );

    $(document).ready(function () {
      $(".block-title").click(function (event) {
        $(this).toggleClass("block-active").next().slideToggle(150);
      });
    });
  }

  handlePageChange(pageNumber) {
    if (pageNumber !== this.state.activePage + 1) {
      this.setState({ activePage: pageNumber - 1 }, function () {
        this.getEarnings(false);
      });
    }
  }

  dateStartInputHandler = (e) => {
    const newDate = moment(e, "DD-MM-YYYY").format();

    this.setState({
      dateStart: e,
      dateStartParam: "&dateStart=" + newDate.split("T")[0],
    });
  };

  dateEndInputHandler = (e) => {
    const newDate = moment(e, "DD-MM-YYYY").format();
    this.setState({
      dateEnd: e,
      dateEndParam: "&dateEnd=" + newDate.split("T")[0],
    });
  };

  submitHandler = (e) => {
    e.preventDefault();
    setDateFilter(
      "earnings",
      this.state.dateStart,
      this.state.dateEnd,
      this.getEarnings.bind(this)
    );
  };

  clearDateInputs() {
    localStorage.removeItem("earningsDateFilter");
    if (this.state.dateStart !== null || this.state.dateEnd !== null) {
      this.setState(
        {
          dateStartParam: "",
          dateEndParam: "",
          dateStart: null,
          dateEnd: null,
        },
        function () {
          this.getEarnings(true);
        }
      );
    }
  }

  changeDateDirection() {
    this.props.changeEarningDateDirection();
    setTimeout(() => {
      this.getEarnings();
    }, 10);
  }

  changeIncomeDirection() {
    this.props.changeEarningIncomeDirection();
    setTimeout(() => {
      this.getEarnings();
    }, 10);
  }

  render() {
    const earnings = this.props.earnings.earnings;
    return (
      <div className="earnings container">
        <div className="row justify-content-center">
          <div className="col">
            <Link to="/earnings/add">
              <button type="button" className="btn btn-primary">
                Add Earning
              </button>
            </Link>
          </div>
        </div>
        <DateRangeFilter
          submitHandler={this.submitHandler}
          dateStart={this.state.dateStart}
          dateEnd={this.state.dateEnd}
          dateStartInputHandler={this.dateStartInputHandler}
          dateEndInputHandler={this.dateEndInputHandler}
          clearDateInputs={this.clearDateInputs.bind(this)}
        />
        <div className="row justify-content-center">
          <div className="col">
            {!this.props.earnings.fetchLoading && !earnings.empty && (
              <ActionsTable
                name="earnings"
                actions={earnings.content}
                colNames={[
                  {
                    colName: "Income",
                    sortFunction: this.changeIncomeDirection.bind(this),
                    sortDirection: this.props.earningSortParams.incomeSort,
                    colValueName: "income",
                  },
                  {
                    colName: "Date",
                    sortFunction: this.changeDateDirection.bind(this),
                    sortDirection: this.props.earningSortParams.dateSort,
                    colValueName: "date",
                  },
                ]}
                classes="earning-table"
              />
            )}
            {this.props.earnings.fetchLoading && <Loader />}
          </div>
        </div>
        <div className="row justify-content-center">
          {!this.props.earnings.fetchLoading &&
            earnings.size &&
            earnings.content.length > 0 && (
              <Pagination
                key={"paginationList"}
                activePage={this.state.activePage + 1}
                itemsCountPerPage={earnings.size}
                totalItemsCount={earnings.totalElements}
                pageRangeDisplayed={3}
                nextPageText="❯"
                prevPageText="❮"
                itemClassPrev="prev-next-arrows"
                itemClassNext="prev-next-arrows"
                onChange={this.handlePageChange.bind(this)}
              />
            )}
          {!this.props.earnings.fetchLoading &&
            earnings.content.length === 0 &&
            "Empty data"}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  fetchEarnings,
  changeEarningIncomeDirection,
  changeEarningDateDirection,
  setActiveRoute,
};

const mapStateToProps = (state) => ({
  earningSortParams: state.earningSortParams,
  earnings: state.earnings,
});

export default connect(mapStateToProps, mapDispatchToProps)(Earnings);
