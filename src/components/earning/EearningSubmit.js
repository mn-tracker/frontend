import React, { Component } from "react";
import { connect } from "react-redux";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { addEarning, setActiveRoute } from "../../redux/actions";
import { isMobile } from "react-device-detect";
import "../../css/addForm.css";

class EarningSubmit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: new Date(),
      income: null,
    };
  }

  componentDidMount() {
    this.props.setActiveRoute("earnings");
  }

  submitHandler = (e) => {
    e.preventDefault();
    this.props.addEarning(this.state);
  };

  inputChangeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  dateInputHandler(date) {
    this.setState({ date: date });
  }

  render() {
    const disabled = this.props.state.earnings.submitLoading;

    return (
      <form onSubmit={this.submitHandler}>
        <div className="form-wrapper">
          <div className="row justify-content-center">
            <label className="date-label">Date</label>
          </div>
          <div className="row justify-content-center">
            <div className="col datetime-col">
              <DatePicker
                selected={this.state.date}
                onChange={(date) => {
                  this.dateInputHandler(date);
                }}
                withPortal={isMobile}
                className="datetime-picker-input"
                dateFormat="MM/dd/yyyy"
                popperPlacement="bottom"
                popperModifiers={{
                  flip: {
                    behavior: ["bottom"], // don't allow it to flip to be above
                  },
                  preventOverflow: {
                    enabled: false, // tell it not to try to stay within the view (this prevents the popper from covering the element you clicked)
                  },
                  hide: {
                    enabled: false, // turn off since needs preventOverflow to be enabled
                  },
                }}
              />
            </div>
          </div>

          <div className="row justify-content-center py-3">
            <input
              required
              type="text"
              pattern="[0-9]*"
              id="income-input"
              className="form-control decimal-input"
              placeholder="Income"
              name="income"
              value={this.state.income}
              onChange={this.inputChangeHandler}
            />
          </div>

          <div className="row justify-content-md-center">
            <button
              disabled={disabled}
              type="submit"
              className="submit-btn btn btn-primary"
            >
              {disabled && <i className="fas fa-circle-notch fa-spin"></i>}
              {disabled && " "}
              Submit
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapDispatchToProps = {
  addEarning,
  setActiveRoute,
};

const mapStateToProps = (state) => ({
  state,
});

export default connect(mapStateToProps, mapDispatchToProps)(EarningSubmit);
