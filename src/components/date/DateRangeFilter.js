import React from "react";
import CustomDatePicker from "./CustomDatePicker";

export default (props) => {
  return (
    <div className="row justify-content-center py-3">
      <div className="col date-filter">
        <div className="block-title">Pick period</div>
        <div className="block-container">
          <form onSubmit={props.submitHandler}>
            <div className="date-start-picker">
              <CustomDatePicker
                placeholder="from"
                date={props.dateStart}
                dateInputHandler={props.dateStartInputHandler}
              />
            </div>
            <div className="date-end-picker">
              <CustomDatePicker
                placeholder="to"
                date={props.dateEnd}
                dateInputHandler={props.dateEndInputHandler}
              />
            </div>
            <button
              type="button"
              className="btn btn-secondary clear-btn"
              onClick={() => props.clearDateInputs()}
            >
              Clear filter
            </button>
            <button type="submit" className="btn btn-success search-btn">
              Search
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};
