import React from "react";
import DatePicker from "react-datepicker";
import { isMobile } from "react-device-detect";

export default (props) => {
  return (
    <DatePicker
      placeholderText={props.placeholder}
      selected={props.date}
      className="date-picker-input"
      dateFormat="MM/dd/yyyy"
      onChange={props.dateInputHandler}
      withPortal={isMobile}
      popperPlacement="bottom"
      popperModifiers={{
        flip: {
          behavior: ["bottom"], // don't allow it to flip to be above
        },
        preventOverflow: {
          enabled: false, // tell it not to try to stay within the view (this prevents the popper from covering the element you clicked)
        },
        hide: {
          enabled: false, // turn off since needs preventOverflow to be enabled
        },
      }}
    />
  );
};
